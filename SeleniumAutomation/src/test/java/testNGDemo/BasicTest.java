package testNGDemo;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BasicTest {
	public WebDriver driver;
	ExtentReports extent;
	ExtentTest extentTest;
	@Test
	public void loginTest() {
		extentTest = extent.startTest("Login");
		driver.findElement(By.id("Email")).sendKeys("manzoormehadi@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("manz@123");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		String expectedUserLink="manzoormehadi@gmail.com";
		String actualUserLink=driver.findElement(By.linkText("manzoormehadi@gmail.com")).getText();
		Assert.assertEquals(actualUserLink, expectedUserLink);
		String pageTitle1=driver.getTitle();
		extentTest.log(LogStatus.PASS, pageTitle1);
        extent.endTest(extentTest);
	}  
	@BeforeClass
	public void launchApplication() {
		extent = new ExtentReports(System.getProperty("user.dir")+"/test-output/manzoor.html");
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		extentTest = extent.startTest("Launch Browser");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.linkText("Log in")).click();
		String pageTitle=driver.getTitle();
		extentTest.log(LogStatus.PASS, pageTitle);
		extent.endTest(extentTest);
	}
	@AfterClass
	public void closeapplication() {
		driver.close();
		extent.flush();
		extent.close();
	}
}
