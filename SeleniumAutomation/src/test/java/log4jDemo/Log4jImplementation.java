package log4jDemo;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Log4jImplementation {

	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		Logger log = Logger.getLogger("TestingloginApplication");
		log.info("Open demo WebShop Application");
		driver.manage().window().maximize();
		log.info("Maximize window size");
		driver.findElement(By.linkText("Log in")).click();
		log.info("click on login link");
		driver.findElement(By.id("Email")).sendKeys("manzoormehadi@gmail.com");
		log.info("enter email");
		driver.findElement(By.id("Password")).sendKeys("manz@123");
		log.info("enter password");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		log.info("click on login");
		driver.close();

	}

}
